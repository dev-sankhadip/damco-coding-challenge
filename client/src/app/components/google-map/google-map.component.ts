import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { AgmInfoWindow } from '@agm/core';
import { PlantService } from 'src/app/service/plant.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit, OnDestroy {
  // google maps zoom level
  zoom: number = 3;
  // initial center position for the map
  lat: number = 50.69624533338217;
  lng: number = -105.75279192789577;
  percentage: number | undefined;
  // top N values
  limitValues: number[] = [5, 10, 15, 20, 50, 100];
  selectedLimitValue: number = 10;
  // clicked state in map
  state: string | undefined;
  // locations of plants
  markers: IMarker[] = []
  private map: any;
  // subscription objects stored for unsubscribe in ngOnDestroy()
  private mapClickListener: any;
  private getPlantSubscription: Subscription = new Subscription();
  private previousInfoWIndow: AgmInfoWindow | undefined;
  // clicked location's latitude and longitude
  private location = {
    lat: "",
    lng: ""
  }


  // show plant details on click
  clickedMarker(state: string, netGeneration: number, infoWindow: AgmInfoWindow) {
    if (this.previousInfoWIndow)
      this.previousInfoWIndow.close();
    this.previousInfoWIndow = infoWindow;
    this.plantService.CalculatePercentage(this.state ? { state } : {})
      .subscribe({
        next: (res: any) => {
          this.percentage = (netGeneration / res[0].totalGeneration) * 100;
        },
        error: (error) => {
          console.error(error);
        }
      })
  }

  // fetch TOP plants on loading of app/change of limit value/view USA's top N plants
  fetchTopPlants() {
    this.getPlantSubscription = this.plantService.GetPlants(this.state ? { ...this.location, top: this.selectedLimitValue } : { top: this.selectedLimitValue })
      .subscribe({
        next: (res: any) => {
          this.markers = res.plants as IMarker[];
        },
        error: (err) => {
          console.error(err);
        }
      })
  }

  // gets called when map get initialized in UI
  mapReadyHandler(map: any): void {
    this.map = map;
    // register to map for click event
    this.mapClickListener = this.map.addListener('click', (e: any) => {
      this.zone.run(() => {
        this.getPlantSubscription = this.plantService.GetPlants({ lat: e.latLng.lat(), lng: e.latLng.lng(), top: this.selectedLimitValue })
          .subscribe({
            next: (res: any) => {
              this.markers = res.plants as IMarker[];
              this.state = res?.state;
              this.previousInfoWIndow = undefined;
              this.location.lat = e.latLng.lat();
              this.location.lng = e.latLng.lng();
            },
            error: (err) => {
              console.error(err);
            }
          })
      });
    });
  }

  constructor(private plantService: PlantService, private zone: NgZone) { }

  ngOnDestroy(): void {
    if (this.getPlantSubscription) this.getPlantSubscription.unsubscribe();
    if (this.mapClickListener) this.mapClickListener.remove();
  }

  ngOnInit(): void {
    this.fetchTopPlants();
  }

}


interface IMarker {
  lat: number;
  lng: number;
  name: string;
  state: string;
  draggable: boolean;
  isOpen: boolean;
  netGeneration: number;
}
