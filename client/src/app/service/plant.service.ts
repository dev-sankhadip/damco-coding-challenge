import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlantService {

  constructor(private http: HttpClient) { }

  // Fetch top N plants
  GetPlants(clause: any = {}) {
    return this.http.post(`${environment.baseURL}/plants`, { ...clause })
  }
  // Calculate net generation percentage
  CalculatePercentage(clause: any = {}) {
    return this.http.post(`${environment.baseURL}/calculate-percentage`, { clause });
  }
}
