<h1 align="center">Damco Coding Challenge</h1>

<br>

## URL

<a href="http://174.129.174.72:4200/" target="_blank" rel="noopener noreferrer">Browse to visualize the annual net generation of power plant</a>
<br>

[![forthebadge](https://forthebadge.com/images/badges/uses-js.svg)](http://forthebadge.com)

<br>

## Steps to run the projects
- Client folder contains Angular and Server folder contains Node.js code
- In Client folder, 
    - <strong>npm i --legacy-peer-deps</strong> to install node_modules
- In Server folder,
    - <strong>npm i</strong> to install node_modules.
    - <strong>npm run dev</strong> to run server in development mode.
    - <strong>npm run build</strong> to generate production build and run server in production mode.
    - <strong>npm run prod</strong> to run in production mode.
    - <strong>npm run clean</strong> to delete build folder content.

## Project Details
- By default, USA's top 10 plants are shown based on annual net generation.
- You can change top N values in dropdown.
- If you click on a USA state, the map will show top 5 plants of the clicked state if exists in database.
- If you click on a plant marker, a tolltip ope which contains annual net generation and percentage.
- Data has been loaded into mongodb from CSV file.
- APP and API are deployed in AW EC2 instance.
- plant collection is created in DB where data was loaded by a parser.
- In case, you want to change API key.
    - In Angular, update in app.module.ts
    - In Node, update in .env file as per environment.

## Backend API Collection in Postman
<a href="https://documenter.getpostman.com/view/17227986/UzQvtkfe">Backend API collection</a>

## Tools used
- NodeJS, Angular
- Database - MongoDB
- Cloud - AWS EC2
- Google Maps to show plants in UI