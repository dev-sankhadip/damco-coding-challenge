import { Router } from 'express';
import { Service } from 'typedi';
import PlantService from '../service/plant.service';

@Service()
export default class ClientRouter {
    constructor(private readonly plantService: PlantService) { }

    SetRouter(router: Router) {
        router.get('/load-plant-data', this.plantService.LoadData);
        router.get('/states', this.plantService.GetAllStates);
        router.post('/plants', this.plantService.GetPlants);
        router.post('/calculate-percentage', this.plantService.CalculateNetGenerationPercentage);
    }
}