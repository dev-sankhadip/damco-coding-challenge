import chai from 'chai';
import chaiHttp from 'chai-http';
import App from '../server'

const app = App.getServer;
const should = chai.should();

chai.use(chaiHttp);


describe('Plants', () => {
    describe('/GET Plants by state', () => {
        it('it should GET top 10 plants of state AK(Alaska)', (done) => {
            const body = {
                state: 'AK',
                top: 10
            }
            chai.request(app)
                .post('/plants')
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('plants');
                    res.body.plants.length.should.be.eql(10);
                    done();
                });
        });
    });
    describe('/GET Plants by location', () => {
        it('it should GET top 10 plants of state AZ(Arizona)', (done) => {
            const body = {
                top: 10,
                lat: 33.70829112401232,
                lng: -111.12644954381736
            }
            chai.request(app)
                .post('/plants')
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('plants');
                    res.body.plants.length.should.be.eql(10);
                    done();
                });
        });
    });
    describe('/GET Net Generation Percentage at Country level', () => {
        it('it should GET percentage of clicked plant at country level', (done) => {
            const body = {
                "clause": {
                }
            }
            chai.request(app)
                .post('/calculate-percentage')
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });
    describe('/GET Net Generation Percentage at State level', () => {
        it('it should GET percentage of clicked plant at AK(Alaska) state level', (done) => {
            const body = {
                "clause": {
                    "state": "AK"
                }
            }
            chai.request(app)
                .post('/calculate-percentage')
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                    done();
                });
        });
    });

    describe('/GET Net Generation Percentage at State level', () => {
        it("it should GET no value as PO state doesn't exist", (done) => {
            const body = {
                "clause": {
                    "state": "PO"
                }
            }
            chai.request(app)
                .post('/calculate-percentage')
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });
})