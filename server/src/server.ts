import 'reflect-metadata';
import express, { Express, Router } from 'express'
import cors from 'cors'
import Container from 'typedi';
import mongoose from 'mongoose';
import { PlantRouter } from './api';

class App {
    private app: Express;
    private router: Router;
    constructor() {
        this.app = express();
        this.router = express.Router();
        this.setMiddleware();
        this.setRoutes();
        this.startApp();
    }

    setMiddleware() {
        require('custom-env').env(true);
        this.app.use(cors({ origin: process.env.APPLICATION_PORT }));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
    }

    setRoutes() {
        this.app.use('/', this.router);
        Container.get(PlantRouter).SetRouter(this.router);
    }

    startApp() {
        mongoose.connect(process.env.DB_URL as string, { dbName: 'damco' })
            .then((res) => {
                const port = process.env.SERVER_PORT || 3000;
                this.app.listen(port, () => {
                    console.log(`Server running on ${port}`);
                });
            })
            .catch((err) => {
                console.log("Database Connection Failed");
                console.error(err);
            })
    }

    get getServer() {
        return this.app;
    }
}

const app = new App();

export default app;