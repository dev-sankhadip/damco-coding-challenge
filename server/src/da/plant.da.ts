import { Service } from 'typedi'
import plant from '../models/plant'
import state from '../models/state';

@Service()
export default class PlantDA {
    // Insert Plant data in DB.
    async InsertPlantData(...data: any) {
        try {
            await plant.insertMany(data);
        } catch (error) {
            console.error(error);
        }
    }

    async GetAllStates() {
        try {
            return await state.find();
        } catch (error) {
            throw error;
        }
    }

    // fetch top N plants from DB.
    async GetPlants(top: string, clause?: any) {
        try {
            return plant.aggregate(
                [
                    {
                        $match: {
                            ...clause
                        }
                    },
                    {
                        $project: {
                            "lat": 1,
                            "lng": 1,
                            "name": 1,
                            "netGeneration": 1,
                            "state": 1,
                            "year": 1
                        }
                    }
                ]
            ).sort({ 'netGeneration': -1 }).limit(parseInt(top));
        } catch (error) {
            throw error;
        }
    }

    // sum of all netgeneration at state/country level
    // fetches at state level if clause contains state property like {state:'AK'} else at country level.
    async CalculateNetGenerationPercentage(clause?: any) {
        try {

            return plant.aggregate(
                [
                    { $match: { ...clause } },
                    { $group: { _id: null, totalGeneration: { $sum: "$netGeneration" } } },
                    { $project: { "_id": 0, "totalGeneration": 1 } }
                ]
            )
        } catch (error) {
            throw error;
        }
    }
}