import axios from "axios";
import { Service } from "typedi";

@Service()
export default class GoogleAPI {
    async FindLocationByLatLong(lat: number, lng: number) {
        try {
            const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?result_type=administrative_area_level_1&latlng=${lat},${lng}&key=${process.env.MAP_API_KEY}`)
            const stateDetails = response.data.results.map((address: any) => {
                return address.address_components.filter((state: any) => {
                    if (state.types.includes('administrative_area_level_1'))
                        return true;
                })
            })
            return stateDetails;
        } catch (error) {
            throw error;
        }
    }
}