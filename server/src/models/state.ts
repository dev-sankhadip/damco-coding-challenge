import mongoose from 'mongoose';

const StateSchema = new mongoose.Schema({
    State: {
        type: String
    },
    Abbrev: {
        type: String
    },
    Code: {
        type: String
    }
}, { collection: "state" })

export default mongoose.model("state", StateSchema)