import mongoose from 'mongoose';

const PlantSchema = new mongoose.Schema({
    code: {
        type: String
    },
    name: {
        type: String
    },
    lng: {
        type: String
    },
    lat: {
        type: String
    },
    yar: {
        type: Number
    },
    state: {
        type: String
    },
    netGeneration: {
        type: Number
    }

}, { collection: "plant" })

export default mongoose.model("plant", PlantSchema);