import { Service } from 'typedi'
import { Request, Response } from 'express'
import { parse } from 'csv-parse'
import fs from 'fs';
import PlantDA from '../da/plant.da';
import GoogleAPI from '../util/google.api';


@Service()
export default class PlantService {
    constructor(private plantDA: PlantDA, private googleAPI: GoogleAPI) { }

    // parser to load data from CSV to database.
    LoadData = async (req: Request, res: Response) => {
        fs.createReadStream(process.env.PLANT_DATA_PATH as string)
            .pipe(parse({ delimiter: ",", from_line: 3 }))
            .on("data", async (row) => {
                const NetGeneration = Number(row[39].replace(/\,/g, ''))
                await this.plantDA.InsertPlantData({
                    code: row[6],
                    name: row[5],
                    lng: row[20],
                    lat: row[19],
                    year: parseInt(row[1]),
                    state: row[2],
                    netGeneration: isNaN(NetGeneration) ? 0 : NetGeneration,
                })
            })
            .on("end", function () {
                return res.status(201).send({
                    message: "Data Read Finished"
                })
            })
            .on("error", function (error) {
                console.log(error.message);
            });
    }

    // fetch all state from DB
    GetAllStates = async (req: Request, res: Response) => {
        try {
            const states = await this.plantDA.GetAllStates()
            res.status(200).send(states);
        } catch (error) {
            res.status(500).send(error);
        }
    }

    // get top N plants at state/country level
    GetPlants = async (req: Request, res: Response) => {
        try {
            const { lat, lng, top } = req.body;
            let clause = {};
            let plants: any[] = [];
            if (lat && lng) {
                // If latitude and longitude is present in body, then find state using google Reverse Geocoding API
                // fetch administrative_area_level_1 type data
                const [stateDetails] = await this.googleAPI.FindLocationByLatLong(lat, lng);
                // Checking is there is any state present in response
                if (stateDetails) {
                    clause = { state: stateDetails[0].short_name }
                }
            }
            // If no state exists on provided locations, return empty plants
            if (lat && lng && Object.keys(clause).length === 0) {
                return res.status(200).send({ plants })
            }
            plants = await this.plantDA.GetPlants(top, clause);
            res.status(200).send({ plants, ...clause });
        } catch (error) {
            console.error(error);
            res.status(500).send(error);
        }
    }

    // calculate percentage on state/country level
    CalculateNetGenerationPercentage = async (req: Request, res: Response) => {
        try {
            const { clause } = req.body;
            const percentage = await this.plantDA.CalculateNetGenerationPercentage(clause);
            res.status(200).send(percentage);
        } catch (error) {
            res.status(500).send(error);
        }
    }

}